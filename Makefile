.PHONY: help

help: ## Help text
	@echo "JO dev environment"
	@echo ""
	@echo "Commands:"
	@echo "  init\t\tInitialise the environment and clone the required repos"
	@echo "  start\t\tStart docker containers"
	@echo "  build\t\tBuild the docker containers"
	@echo "  stop\t\tStop docker containers"
	@echo "  refresh\tRefresh the environment without losing images or volumes"
	@echo "  destroy\tDestroy and cleanup the environment"
	@echo ""
	@echo ""

.DEFAULT_GOAL := help
	
## Tasks
init:
	@[ $(REPO_ROOT) ] || ( echo "\nPlease specify your REPO_ROOT.\neg; make init /path/to/your/repo/root\n\n"; exit 1)

	# Set repo root dir as ENV var
	export LOCAL_PATH_TO_REPOS=$(REPO_ROOT)

	@echo "Cloning repos..."
	cd $(REPO_ROOT); \
	git clone git@bitbucket.org:jamieoliver/docker-jo-dev.git; \
	git clone git@bitbucket.org:jamieoliver/jo-com-core.git; \
	git clone git@bitbucket.org:jamieoliver/jocom-varnish.git; \
	git clone git@bitbucket.org:jamieoliver/jocom-drinkstube.git; \
	git clone git@bitbucket.org:jamieoliver/recipe-database.git; \
	git clone git@bitbucket.org:jamieoliver/jo-solr.git; \
	git clone git@bitbucket.org:jamieoliver/jo-spud.git
	@echo "done!"
	@echo ""

	@echo "Grabbing environment files..."
	cd $(REPO_ROOT)/docker-jo-dev; \
	mc cp jo-dev-tubs/jo-dev/docker-jo-dev/env.tar.bz2 .; \
	tar Jxf env.tar.bz2; \
	rm env.tar.bz2

	cd $(REPO_ROOT)/jo-com-core; \
	mc cp jo-dev-tubs/jo-dev/jo-com-core/env.tar.bz2 .; \
	tar Jxf env.tar.bz2; \
	rm env.tar.bz2

	cd $(REPO_ROOT)/jo-spud/; \
	mc cp jo-dev-tubs/jo-dev/jo-spud/env.tar.bz2 .; \
	tar Jxf env.tar.bz2; \
	rm env.tar.bz2

	cd $(REPO_ROOT)/jocom-drinkstube; \
	mc cp jo-dev-tubs/jo-dev/jocom-drinkstube/env.tar.bz2 .; \
	tar Jxf env.tar.bz2; \
	rm env.tar.bz2

	cd $(REPO_ROOT)/new-signup-api; \
	mc cp jo-dev-tubs/jo-dev/new-signup-api/env.tar.bz2 .; \
	tar Jxf env.tar.bz2; \
	rm env.tar.bz2

	cd $(REPO_ROOT)/recipe-database; \
	mc cp jo-dev-tubs/jo-dev/recipe-database/env.tar.bz2 .; \
	tar Jxf env.tar.bz2; \
	rm env.tar.bz2
	# Download relevant .env files and put into the right repos
	@echo "done!"
	@echo ""
	@echo "You may now run: 'make start' to start the environment"

dbs:
	@[ $(REPO_ROOT) ] || ( echo "\nPlease specify your REPO_ROOT.\neg; make init /path/to/your/repo/root\n\n"; exit 1)

	# Set repo root dir as ENV var
	export LOCAL_PATH_TO_REPOS=$(REPO_ROOT)

	@echo "Grabbing and populating default databases..."
	# jo-com-core
	cd $(REPO_ROOT)/jo-com-core; \
	mc cp jo-dev-tubs/jo-dev/jo-com-core/jo_local_Dump20210112.sql.bz2 .; \
	bzip2 -d jo_local_Dump20210112.sql.bz2

	cd $(REPO_ROOT)/docker-jo-dev; \
	docker-compose exec mysql mysql -uroot -proot -e 'CREATE DATABASE IF NOT EXISTS jo_com CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;'; \
	cat ../jo-com-core/jo_local_Dump20210112.sql | docker-compose exec -T mysql mysql -uroot -proot jo_com

	rm $(REPO_ROOT)/jo-com-core/jo_local_Dump20210112.sql

	# jocom-drinkstube
	cd $(REPO_ROOT)/jocom-drinkstube; \
	mc cp jo-dev-tubs/jo-dev/jocom-drinkstube/drinkstube_local_Dump20210112.sql.bz2 .; \
	bzip2 -d drinkstube_local_Dump20210112.sql.bz2

	cd $(REPO_ROOT)/docker-jo-dev; \
	docker-compose exec mysql mysql -uroot -proot -e 'CREATE DATABASE IF NOT EXISTS jo_drinkstube CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;'; \
	cat ../jocom-drinkstube/drinkstube_local_Dump20210112.sql | docker-compose exec -T mysql mysql -uroot -proot jo_drinkstube

	rm $(REPO_ROOT)/jocom-drinkstube/drinkstube_local_Dump20210112.sql
	
	@echo "done!"
	@echo ""

	@echo "Adding SSL certificates..."
	# Add SSL certificates
	@echo "done!"

start:
	cd ../docker-jo-dev; \
	docker-compose up -d; \
	cd ../jo-spud; \
	docker-compose up -d

build:
	cd ../docker-jo-dev; \
	docker-compose build; \
	cd ../jo-spud; \
	docker-compose build

stop:
	cd ../docker-jo-dev; \
	docker-compose stop;
	cd ../jo-spud; \
	docker-compose stop

refresh:
	cd ../docker-jo-dev; \
	docker-compose down; \
	docker-compose up -d --build; \
	cd ../jo-spud; \
	docker-compose down; \
	docker-compose up -d --build

destroy:
	cd ../docker-jo-dev; \
	docker-compose down --rmi all --volumes --remove-orphans; \
	cd ../jo-spud; \
	docker-compose down --rmi all --volumes --remove-orphans;

## Start/stop Individual services
# Spud - User data service
start-spud:
	cd ../jo-spud; \
	docker-compose up -d

stop-spud:
	cd ../jo-spud; \
	docker-compose stop

build-spud:
	cd ../jo-spud; \
	docker-compose build

migrate-spud:
	cd ../jo-spud; \
	docker-compose exec -e XDEBUG_MODE=off jo-spud-php php artisan migrate --seed

migrate-spud-fresh:
	cd ../jo-spud; \
	docker-compose exec -e XDEBUG_MODE=off jo-spud-php php artisan migrate:fresh --seed

# Main - all other dev sites
start-main:
	cd ../docker-jo-dev; \
	docker-compose up -d

stop-main:
	cd ../jo-spud; \
	docker-compose stop

build-main:
	cd ../jo-spud; \
	docker-compose build

## Nicities
list-containers:
	@[ $(REPO_ROOT) ] || ( echo "\nPlease specify your REPO_ROOT.\neg; make init /path/to/your/repo/root\n\n"; exit 1)

	# Set repo root dir as ENV var
	@export LOCAL_PATH_TO_REPOS=$(REPO_ROOT)

	@echo "Main containers:"
	@cd $(REPO_ROOT)/docker-jo-dev; \
	docker-compose ps
	@echo ""
	@echo "";

	@echo "Spud containers:"
	@cd $(REPO_ROOT)/jo-spud; \
	docker-compose ps
	@echo ""
	@echo ""
